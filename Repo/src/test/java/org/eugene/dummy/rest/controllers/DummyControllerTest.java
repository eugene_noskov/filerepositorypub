/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.dummy.rest.controllers;

import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.resource.TestObjectResource;
import org.eugene.dummy.persistence.services.TestObjectService;
import org.eugene.dummy.persistence.exceptions.TestObjectsAlreadyExistsException;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.hamcrest.CoreMatchers.nullValue;
//import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.ModelResultMatchers.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author User
 */
public class DummyControllerTest {
    
    @Mock
    private TestObjectService objectService;

    @InjectMocks
    private DummyController controller;
    
    
    private MockMvc mockMvc;

    private ArgumentCaptor<TestObjectResource> testObjectArgumentCaptor;




    public DummyControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        //MockMvc : Circular view path [view]: would dispatch back to the current handler URL [/view] again
        
 //       mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        //viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");
 
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                                 .setViewResolvers(viewResolver)
                                 .build();
        // - - - - - - 
        
        testObjectArgumentCaptor = ArgumentCaptor.forClass(TestObjectResource.class);
        


    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTestObject() throws Exception {
        mockMvc.perform(post("/testObject")
                .content("{\"testField\":\"Hello\"}")
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("$.testField", is("Hello"))) //.andDo(print())
                ;
    }
    

    @Test
    public void testTestExistingObject() throws Exception {
        TestObject testObject = new TestObject();
        testObject.setId(1L);
        testObject.setTestField("some field");
        testObject.setPassword("123");

        Mockito.when(objectService.find(1L)).thenReturn(testObject);

        //TestObject t = objectService.find(1L);
        mockMvc.perform(get("/testObject/1"))
                .andDo(print())
                .andExpect(jsonPath("$.testField", is(testObject.getTestField())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/testObject/1"))))
                .andExpect(status().isOk());
    }
    
    @Test
    public void testNonExistingTestObject() throws Exception {

        Mockito.when(objectService.find(1L)).thenReturn(null);
        
        TestObject t = objectService.find(1L);
        
        mockMvc.perform(get("/testObject/1"))
                .andExpect(status().isNotFound())
                ;
    }


    @Test
    public void testCreateTestObject() throws Exception {

        //Проверим, что функция вообще вызывалась
        Mockito.when(objectService.create(Mockito.any(TestObjectResource.class))).thenReturn(1L);

        mockMvc.perform(post("/createTestObject")
                .content("{\"testField\":\"Hello\", \"password\":\"123\"}")
                .contentType(MediaType.APPLICATION_JSON)
        )
                //                .andDo(print())
                //                .andExpect(jsonPath("$.testField", is(testObject.getTestField())))
                //                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/testObject/1"))))
                .andExpect(status().isOk());

        Mockito.verify(objectService).create(testObjectArgumentCaptor.capture());

        String pass = testObjectArgumentCaptor.getValue().getPassword();

        //Humcrest
        assertThat(pass, nullValue());
        assertThat(pass, is(nullValue()));
        //junit
        assertNull(pass);

        //.andExpect(header().string("Location", endsWith("/blogs/1")))
    }
    
    @Test
    public void testObjectIsAlredyExists() throws Exception {
        //3) проверим, что выбрасывается экцепшн
        Mockito.when(objectService.create(Mockito.any(TestObjectResource.class)))
                .thenThrow(new TestObjectsAlreadyExistsException("Test objectis alredy exists"))
                ;

        mockMvc.perform(post("/createTestObject")
                .content("{\"testField\":\"Hello\", \"password\":\"123\"}")
                .contentType(MediaType.APPLICATION_JSON)
        )
                //                .andDo(print())
                //                .andExpect(jsonPath("$.testField", is(testObject.getTestField())))
                //                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/testObject/1"))))
                .andExpect(status().isConflict());

    }

}
