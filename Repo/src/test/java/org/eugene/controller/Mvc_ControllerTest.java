/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.controller;

import org.eugene.rest.controller.Mvc_Controller;
import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.resource.TestObjectResource;
import org.eugene.dummy.persistence.services.TestObjectService;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.hamcrest.CoreMatchers.nullValue;
//import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.ModelResultMatchers.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

public class Mvc_ControllerTest {
    
   @Mock
    private TestObjectService objectService;

    @InjectMocks
    private Mvc_Controller controller;
    
    
    private MockMvc mockMvc;

    private ArgumentCaptor<TestObjectResource> testObjectArgumentCaptor;



    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        MockitoAnnotations.initMocks(this);
        
        //MockMvc : Circular view path [view]: would dispatch back to the current handler URL [/view] again
        
 //       mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        //viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");
 
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                                 .setViewResolvers(viewResolver)
                                 .build();
        // - - - - - - 
        
        testObjectArgumentCaptor = ArgumentCaptor.forClass(TestObjectResource.class);
        

    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testGreeting() throws Exception {
        
        mockMvc.perform(get("/greeting"))
                //.andDo(print())
                ;
        
          assertEquals(1, 1);
    }

}
