package org.eugene.utils;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpHeaders;


public class AppUtils {

    public static String getFileMD5Sum(String path) throws NoSuchAlgorithmException, FileNotFoundException, IOException {

        MessageDigest md;
        StringBuffer sb = new StringBuffer();
        

        md = MessageDigest.getInstance("MD5");

        try (FileInputStream fis = new FileInputStream(path)) {
            byte[] dataBytes = new byte[1024];

            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            };
            byte[] mdbytes = md.digest();

            //convert the byte to hex format method 1
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

//            //convert the byte to hex format method 2
//            StringBuffer hexString = new StringBuffer();
//            for (int i = 0; i < mdbytes.length; i++) {
//                String hex = Integer.toHexString(0xff & mdbytes[i]);
//                if (hex.length() == 1) {
//                    hexString.append('0');
//                }
//                hexString.append(hex);
//            }
//            System.out.println("Digest(in hex format):: " + hexString.toString());
        }
        
        return sb.toString();

    }
    
    public static HttpHeaders setHTTPHeader(HttpHeaders headers, String section, String key, String value){
        
        List<String> sections = headers.get(section);
        
        
        if(sections == null){
            if(value != null && value.length() != 0){
                headers.set(section, key+"="+"\""+value+"\";");
            }else if(value != null && value.length() == 0){
                headers.set(section, key+";");
            }else{
                headers.set(section, key);
            }
        }else{
            int sectionsQty = sections.size();
            String currentHeader = sections.get(sectionsQty - 1);
            if(value != null && value.length() != 0){
                headers.set(section, currentHeader+" "+key+"="+"\""+value+"\";");
            }else{
                headers.set(section, currentHeader+" "+key+";");
            }
        }

        return headers;
    }

    public static String convertDateTimeTo1CDateString(LocalDateTime dateTime) {
        //LocalDateTime a = LocalDateTime.now();
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
        //String now = "2016-11-09 10:30";
        //LocalDateTime a = LocalDateTime.parse(now, formatter);
        //System.out.println(" - - - "+a.format(formatter));
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

        return dateTime.format(formatter);
    }

}
