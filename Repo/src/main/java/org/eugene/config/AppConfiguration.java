package org.eugene.config;

import java.io.File;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.log4j.Log4j2;
import org.eugene.dummy.CompInterface;
import org.eugene.persistence.entity.Event;
import org.eugene.persistence.entity.enums.EventEnum;
import org.eugene.persistence.repository.EventRepository;
import org.eugene.persistence.repository.ObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;


@Configuration
@ImportResource("classpath:applicationContext.xml")
public class AppConfiguration {

//    @Autowired
//    private ApplicationContext appContext;
    
    @Autowired
    private AppProperties appProperties;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private CompInterface compInterface;
    
    //private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @Bean
    String initializeEnvirement() throws Exception {
        
        //AppProperties appProperties = appContext.getBean("appProperties", AppProperties.class);
        if (Files.notExists(Paths.get(appProperties.getObjectsFolderLocation()), NOFOLLOW_LINKS)) {
            Path path = Paths.get(appProperties.getObjectsFolderLocation());
            new File(path.toString()).mkdir();
        }
        
        if (Files.notExists(Paths.get(appProperties.getObjectsArchiveFolderLocation()), NOFOLLOW_LINKS)) {
            Path path = Paths.get(appProperties.getObjectsArchiveFolderLocation());
            new File(path.toString()).mkdir();
        }

        // - - -
        
        List<Event> events = (List<Event>) eventRepository.findAll();

        for (EventEnum eventEnum : EventEnum.values()) {
            Event foundEvent = events.stream().filter(event->event.getName().equals(eventEnum.toString())).findAny()                                      // If 'findAny' then return found
                .orElse(null);
            if(foundEvent == null)
                eventRepository.save(new Event(eventEnum.toString()));
        }

        // - - - 
        //LocalDateTime a = LocalDateTime.now();
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
        //String now = "2016-11-09 10:30";
        //LocalDateTime a = LocalDateTime.parse(now, formatter);
        //System.out.println(" - - - "+a.format(formatter));

        return null;
    }
    
    @Bean
    @Profile({"profile_A", "profile_B"}) //- это тоже будет работать
    String test(){
        //System.out.println(compInterface.sayHello());
        return null;
    }
    
}
