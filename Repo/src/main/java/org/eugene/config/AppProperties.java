package org.eugene.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:.//${app_start_profile}.properties") //configuration
//@PropertySource("file:.//prod.properties") //configuration

//@ConfigurationProperties("objects")
//@DependsOn("startProperties")
//@PropertySource("file:.//#{systemProperties['app_active_profile']}.properties") //configuration
public class AppProperties {

    @Value("${objects.folder-location}")
    private String objectsFolderLocation;

    @Value("${objects.archive-folder-location}")
    private String objectsArchiveFolderLocation;

    public String getObjectsArchiveFolderLocation() {
        return objectsArchiveFolderLocation;
    }

    public void setObjectsArchiveFolderLocation(String objectsArchiveFolderLocation) {
        this.objectsArchiveFolderLocation = objectsArchiveFolderLocation;
    }

    public String getObjectsFolderLocation() {
        return objectsFolderLocation;
    }

    public void setObjectsFolderLocation(String objectsFolder) {
        this.objectsFolderLocation = objectsFolder;
    }
    
//    @PostConstruct
//    public void init() {
//        System.out.println(System.getProperty("app_start_profile"));
//    }
}
