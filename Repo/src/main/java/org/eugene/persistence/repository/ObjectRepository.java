package org.eugene.persistence.repository;

import org.eugene.persistence.entity.Object;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectRepository extends CrudRepository<Object, String>, ObjectRepositoryCustom{
    
}
