package org.eugene.persistence.repository;

import java.util.List;
import org.eugene.persistence.entity.EventLog;
import org.eugene.persistence.entity.Object;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventLogRepository 
        extends JpaRepository<EventLog, Long>
{

    List<EventLog> findLast20EventsOfObject(@Param("obj") Object obj); //
    
    @Query("SELECT e FROM EventLog e where e.object = :obj ORDER BY e.dateTime DESC")
    public Page<EventLog> findEventLogTopN(Pageable pageable, @Param("obj") Object obj);
    
}
