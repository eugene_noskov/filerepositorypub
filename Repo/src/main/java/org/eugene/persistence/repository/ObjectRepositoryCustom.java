/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.persistence.repository;

import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;

//@NoRepositoryBean
public interface ObjectRepositoryCustom {
    public List<org.eugene.persistence.entity.Object> someCustomMethod(String name);
}
