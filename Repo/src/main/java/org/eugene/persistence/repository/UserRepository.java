package org.eugene.persistence.repository;

import org.eugene.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
 
    User findByUsername(String username);
}