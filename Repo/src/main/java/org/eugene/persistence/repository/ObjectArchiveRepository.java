package org.eugene.persistence.repository;

import org.eugene.persistence.entity.Object;
import org.eugene.persistence.entity.ObjectArchive;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectArchiveRepository extends CrudRepository<ObjectArchive, String>{
    
}
