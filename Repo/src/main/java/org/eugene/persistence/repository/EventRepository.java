package org.eugene.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository 
        extends CrudRepository<org.eugene.persistence.entity.Event, String>
{
    
}
