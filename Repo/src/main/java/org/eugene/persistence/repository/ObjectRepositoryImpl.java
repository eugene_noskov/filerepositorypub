package org.eugene.persistence.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ObjectRepositoryImpl implements ObjectRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<org.eugene.persistence.entity.Object> someCustomMethod(String guid) {
        return this.entityManager.
                createQuery("SELECT o FROM Object o where o.GUID like :guid") //
                .setParameter("guid", guid)
                .getResultList();
        //query.setMaxResults(1).getResultList();
    }
}
