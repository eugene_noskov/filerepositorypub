package org.eugene.persistence.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(
        name = "OBJECT"
//        ,indexes = {
//            @Index(name = "IDX_MD5",  columnList="MD5")
//        }
)
@Data
public class Object {

    public Object() {
    }
    
    @Id
    String GUID;
    
    //@Column(name = "name_path", nullable = false)
    String fileNamePath;
    
    String fileNameOriginal;
    
    String MD5;
    
    String description;
    
    LocalDateTime dateLastChange;
    
    @OneToOne
    User userLastChange;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="object")
    //@OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "object") //orphanRemoval = true, 
    //@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "object") //orphanRemoval = true, 
    //@OnDelete(action = OnDeleteAction.NO_ACTION)
    private List<EventLog> eventLog;
    
}
