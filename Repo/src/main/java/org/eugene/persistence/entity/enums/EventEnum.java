package org.eugene.persistence.entity.enums;

public enum EventEnum {
    CREATED, UPDATED, DELETED
}
