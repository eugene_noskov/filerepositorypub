package org.eugene.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor @RequiredArgsConstructor
public class Event {

  @Id @NonNull
  private String name;
    
//  @OneToMany(mappedBy="event")
//  private List<EventLog> eventLog;

}
