package org.eugene.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Data
//@RequiredArgsConstructor
//@AllArgsConstructor
@NoArgsConstructor

@NamedQuery(name = "EventLog.findLast20EventsOfObject", query = "SELECT e FROM EventLog e where e.object = :obj ORDER BY e.dateTime DESC") //

public class EventLog {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn( name = "OBJECT_ID", referencedColumnName="GUID", nullable = true )
    private Object object;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="EVENT_ID", referencedColumnName="NAME")
    @NonNull
    private Event event;
    
    @ManyToOne
    @PrimaryKeyJoinColumn(name="USER_ID", referencedColumnName="USERNAME")
    @NonNull
    private User user;
    
    @NonNull
    private LocalDateTime dateTime;
    
    @NonNull
    private String decription;

    public EventLog(Object object, Event event, User user, LocalDateTime dateTime, String decription) {
        this.object = object;
        this.event = event;
        this.user = user;
        this.dateTime = dateTime;
        this.decription = decription;
    }
    
    
}
