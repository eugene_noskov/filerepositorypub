package org.eugene.persistence.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
public class ObjectArchive {

    public ObjectArchive() {
    }
    
    @Id
    String GUID;
    
    String GUID_Original;
    
    String fileNamePath;
    
    String fileNameOriginal;
    
    String MD5;
    
    @OneToOne
    private EventLog eventLog;
}
