package org.eugene.persistence.entity;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
public class User {
 
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
 
    @Id
    @Column(nullable = false, unique = true)
    private String username;
 
    private String password;
    
    @OneToMany(mappedBy="user", fetch = FetchType.EAGER) //
    private Collection<Role> roles;

}
