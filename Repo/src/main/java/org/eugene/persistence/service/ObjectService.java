package org.eugene.persistence.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.RuntimeErrorException;
import lombok.extern.apachecommons.CommonsLog;
import org.eugene.config.AppProperties;
import org.eugene.persistence.entity.EventLog;
import org.eugene.persistence.repository.ObjectRepository;
import org.eugene.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import org.eugene.persistence.entity.Object;
import org.eugene.persistence.exception.ObjectsNotFoundException;
import org.eugene.utils.RequestUtils;

@Service
@CommonsLog
public class ObjectService {

    @Autowired
    LocalContainerEntityManagerFactoryBean entityManagerFactory;

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    RequestUtils sessionUtils;

    @Autowired
    private AppProperties appProperties;

    public Object saveNew(byte[] bytes, String fileNameOriginal, String description, String UUID) throws FileNotFoundException, IOException, NoSuchAlgorithmException {

        Object newObject = new Object();

        newObject.setDescription(description);
        newObject.setGUID(UUID);
        newObject.setDateLastChange(LocalDateTime.now());
        newObject.setUserLastChange(sessionUtils.getCurrentUser());

        newObject.setFileNameOriginal(fileNameOriginal);
        
        //_@_ - divider, for easy way to find out file name later
        newObject.setFileNamePath(newObject.getFileNameOriginal() + "_@_" + newObject.getGUID());

        Path path = Paths.get(appProperties.getObjectsFolderLocation() + "/" + newObject.getFileNamePath());

        try (FileOutputStream df = new FileOutputStream(path.toString())) {
            df.write(bytes);
        }

        newObject.setMD5(AppUtils.getFileMD5Sum(path.toString()));

        objectRepository.save(newObject);
        
        return newObject;
    }
    
    public Object updateExisted(byte[] decode, String guid, String fileNameOriginal) throws ObjectsNotFoundException, RuntimeException {

        Object object = objectRepository.findOne(guid);
        

        if (object == null) {
            throw new ObjectsNotFoundException("Объект с GUID " + guid + " не найден в базе данных");
        }

        
        
        
        //eventLogService.saveNew(EventEnum.CREATED, object);
        //Deleting old file
        String filePath = appProperties.getObjectsFolderLocation() + "/" + object.getFileNamePath();
        File file = new File(filePath);
        
        
        
        
        //Moving file to achive
        if (!file.exists()) { // || !file.delete()
            RuntimeException ex = new RuntimeException("Не удалось удалить старый файл: " + filePath);
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex);
        } else {
            Path pathExisted = Paths.get(filePath);
            String archive_uuid = UUID.randomUUID().toString();
            Path pathInArchive = Paths.get(appProperties.getObjectsArchiveFolderLocation()+"/"+object.getFileNameOriginal() + "_@_" + archive_uuid);
            try {
                Files.move(pathExisted, pathInArchive, REPLACE_EXISTING);
            } catch (IOException ex) {
                RuntimeException ex_new = new RuntimeException("Не удалось переместить файл в архив: " + pathInArchive.toString());
                Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex_new);
            }
        }
        
        

        object.setFileNameOriginal(fileNameOriginal);
        
        //_@_ - divider, for easy way to find out file name later
        object.setFileNamePath(object.getFileNameOriginal() + "_@_" + object.getGUID());
        
        object.setDateLastChange(LocalDateTime.now());
        object.setUserLastChange(sessionUtils.getCurrentUser());
        
        objectRepository.save(object);


        // - - - File operation
        //appProperties = appContext.getBean("appProperties", AppProperties.class);
        filePath = appProperties.getObjectsFolderLocation() + "/" + object.getFileNamePath();

        try (FileOutputStream df = new FileOutputStream(filePath)) {
            df.write(decode);
        } catch (IOException ex) {
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, "Couldn't save file into storage folder", ex);
            return null;
        }

        try {
            object.setMD5(AppUtils.getFileMD5Sum(filePath));
        } catch (NoSuchAlgorithmException|IOException ex) {
            Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, "Can't get MD5 summ from received file", ex);
            return null;
        }

        

        return object;

    }
    
    public void removeByID(String guid) throws ObjectsNotFoundException{
        Object object = objectRepository.findOne(guid);
        
        for (EventLog eventLog : object.getEventLog()) {
            eventLog.setObject(null);
        }
        
        
        
        if(object == null)
            throw new ObjectsNotFoundException("Object not found" + guid);
        
        objectRepository.delete(object);
    }

}
