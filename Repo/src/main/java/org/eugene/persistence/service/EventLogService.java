package org.eugene.persistence.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.eugene.persistence.entity.Event;
import org.eugene.persistence.entity.Object;
import org.eugene.persistence.entity.EventLog;
import org.eugene.persistence.entity.enums.EventEnum;
import org.eugene.persistence.entity.Object;
import org.eugene.persistence.repository.EventLogRepository;
import org.eugene.persistence.repository.EventRepository;
import org.eugene.persistence.repository.ObjectRepository;
import org.eugene.rest.dto.EventLogDto;
import org.eugene.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class EventLogService {
    
    @Autowired
    ObjectRepository objectRepository;

    @Autowired
    EventRepository eventRepository;
    
    @Autowired
    EventLogRepository eventLogRepository;

    @Autowired
    RequestUtils sessionUtils;

//    @PersistenceContext
//    private EntityManager entityManager;

    public void saveNew(EventEnum eventEnum, Object object, String description){
        
        Event event = eventRepository.findOne(eventEnum.toString());
        EventLog eventLog = new EventLog(
                object, 
                event, 
                sessionUtils.getCurrentUser(), 
                LocalDateTime.now(),
                description
        );
        eventLogRepository.save(eventLog);
    }
    
    public List<EventLog> findLast20EventsOfObject(String guid){
        
        Object obj = objectRepository.findOne(guid);
        
        List<EventLog> eventLogs = eventLogRepository.findLast20EventsOfObject(obj);
        
        return eventLogs;
    }
    
    @Transactional
    public List<EventLogDto> findMyModelTop20(String guid) {
        
        Object obj = objectRepository.findOne(guid);
        
        Page<EventLog> eventLogsPage  = eventLogRepository.findEventLogTopN(new PageRequest(0, 20), obj);
        
        List<EventLogDto> eventLogs = new ArrayList<>();
        for (EventLog eventLog : eventLogsPage) {
            eventLogs.add(EventLogDto.convertToDto(eventLog));
        }
        
        return eventLogs;
    }
    
}
