package org.eugene.persistence.exception;

public class ObjectsNotFoundException extends RuntimeException{

    public ObjectsNotFoundException(String message) {
        super(message);
    }
    
    public ObjectsNotFoundException(Throwable cause) {
        super(cause);
    }
    
}
