/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.dummy.rest.controllers;

import org.eugene.dummy.rest.resource.asm.TestObjectResourceAsm;
import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.resource.TestObjectResource;
import org.eugene.dummy.persistence.services.TestObjectService;
import org.eugene.dummy.persistence.exceptions.TestObjectsAlreadyExistsException;
import org.eugene.dummy.rest.exceptions.ConflictException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author User
 */
public class DummyController {

    private TestObjectService objectService;
   

    public DummyController(TestObjectService objectService) {
        this.objectService = objectService;
    }
    
    @RequestMapping("/testObject/{id}")
    public ResponseEntity<TestObjectResource> testExistingObject(
            @PathVariable Long id
    ) {
        TestObject testObject = objectService.find(id);

        if (testObject == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        TestObjectResourceAsm asm = new TestObjectResourceAsm();
        return new ResponseEntity(asm.toResource(testObject), HttpStatus.OK);
    }

    @RequestMapping(value = "/createTestObject", method = RequestMethod.POST)
    public ResponseEntity<Long> createTestObject(
            @RequestBody TestObjectResource object
    ) {

         Long id = null;
        try {
           id = objectService.create(object);
        } catch (TestObjectsAlreadyExistsException e) {
            throw new ConflictException(e);
        }
        
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(value = "/testObject", method = RequestMethod.POST)
    public @ResponseBody
    TestObjectResource testObject(
            @RequestBody TestObjectResource testObject
    ) {

        return testObject;
    }

}
