package org.eugene.dummy.rest.resource.asm;

import org.eugene.rest.controller.Mvc_Controller;
import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.controllers.DummyController;
import org.eugene.dummy.rest.resource.TestObjectResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

public class TestObjectResourceAsm extends ResourceAssemblerSupport<TestObject, TestObjectResource> {

    public TestObjectResourceAsm() {
        super(Mvc_Controller.class, TestObjectResource.class);
    }

    @Override
    public TestObjectResource toResource(TestObject t) {
        TestObjectResource objectResource = new TestObjectResource();
        objectResource.setTestField(t.getTestField());
        objectResource.setPassword(t.getPassword());
        
        Link link = linkTo(methodOn(DummyController.class).testExistingObject(t.getId())).withSelfRel();
        
        //Можно так, если на самом классе указан @RequestMapping("/testObject"),
        //а на методе @RequestMapping("/{id}")
        //Link link = linkTo(Mvc_Controller.class).slash(objectResource.getId()).withSelfRel();
        
        objectResource.add(link);
        
        return objectResource;
    }

}
