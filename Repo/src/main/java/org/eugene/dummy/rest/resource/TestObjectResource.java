package org.eugene.dummy.rest.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

//@Data
public class TestObjectResource extends ResourceSupport{

    private String testField;

    public String getTestField() {
        return testField;
    }

    @JsonProperty
    public void setTestField(String testField) {
        this.testField = testField;
    }
    
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    //@JsonIgnore //не отдавать в REST, вернет NULL
     private String password;
    
}
