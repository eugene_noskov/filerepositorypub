/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.dummy;

/**
 *
 * @author User
 */
public class Calculator {
    
    public int abs(int num){
        
        if(num >= 0)
            return num;
        else if(num < 0)
            return -num;
        
        return 0;
        
    }
    
}
