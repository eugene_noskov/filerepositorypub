package org.eugene.dummy;

import lombok.Data;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("profile_A")
@Component
@Data
public class ComponentA implements CompInterface{
    String helloString = "Hello from Component A";

    @Override
    public String sayHello() {
        return(helloString);
    }
    
    
}
