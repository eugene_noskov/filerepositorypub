package org.eugene.dummy.persistence.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class TestObjectsAlreadyExistsException extends RuntimeException{

    public TestObjectsAlreadyExistsException(String message) {
        super(message);
    }

    
    public TestObjectsAlreadyExistsException(Throwable cause) {
        super(cause);
    }
    
}
