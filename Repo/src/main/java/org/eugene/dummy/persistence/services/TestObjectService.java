/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eugene.dummy.persistence.services;

import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.resource.TestObjectResource;

public interface TestObjectService {
    public TestObject find(Long id);
    public Long create(TestObjectResource testObjectResource);
}
