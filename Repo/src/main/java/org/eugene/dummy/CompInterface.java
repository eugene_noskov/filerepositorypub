package org.eugene.dummy;

import org.springframework.stereotype.Component;

@Component
public interface CompInterface {
    public String sayHello();
}
