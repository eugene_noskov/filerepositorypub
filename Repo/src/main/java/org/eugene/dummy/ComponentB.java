package org.eugene.dummy;

import lombok.Data;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("profile_B")
@Component
@Data
public class ComponentB implements CompInterface{
    String helloString = "Hello from Component B";

    @Override
    public String sayHello() {
        return(helloString);
    }
    
    
}
