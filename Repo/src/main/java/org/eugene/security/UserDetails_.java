package org.eugene.security;

import org.eugene.persistence.entity.Role;
import org.eugene.persistence.entity.User;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetails_ implements UserDetails {

    private User user;

    public UserDetails_(User user) {
        this.user = user;
    }

    public UserDetails_() {
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {

        List<Role> roles = null;

        if (user != null) {
            roles = (List<Role>) user.getRoles();
        }

        Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>(roles.size());

        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRole()));
        }

        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        if (this.user == null) {
            return null;
        }
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        //return this.user.isAccountNonExpired();
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        //return this.user.isAccountNonLocked();
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //return this.user.isCredentialsNonExpired();
        return true;
    }

    @Override
    public boolean isEnabled() {
        //return this.user.isEnabled();
        return true;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "CustomUserDetails [user=" + user + "]";
    }

}
