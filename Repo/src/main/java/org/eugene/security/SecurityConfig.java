package org.eugene.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserDetService userDetailsService ;

    @Autowired
    public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {

        builder.userDetailsService(userDetailsService); 
        
//        builder.inMemoryAuthentication()
//				.withUser("user").password("user").roles("USER")
//				.and()
//				.withUser("admin").password("admin1").roles("ADMIN");

    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests().antMatchers("/").permitAll()
                .and()
                .authorizeRequests().antMatchers("/greeting").permitAll()  
                .and()
                .httpBasic()

                

                .and()
                .authorizeRequests()
                .antMatchers("/h2/", "/h2/**").hasRole("ADMIN")
                .antMatchers("/object/put/**").hasRole("DEVELOPER")
                .antMatchers("/object/setUpdate/**").hasRole("DEVELOPER")
                .antMatchers("/object/remove/**").hasRole("DEVELOPER")
                .antMatchers("/object/getInfo/**").hasRole("DEVELOPER")
                
                .antMatchers("/object/**").authenticated()
                
                
                
                
                
                
                //Permit everything to everuone
                //.antMatchers("/**").permitAll()
                
//                .antMatchers("/h2/", "/h2/**").hasRole("ADMIN").anyRequest().authenticated()
//                .antMatchers("/object/put/**").access("hasRole('ADMIN')").anyRequest().authenticated()
//                
//                .antMatchers("/object/**").access("hasRole('USER')").anyRequest().authenticated()









//                .antMatchers("/").permitAll()
//                .antMatchers("/**").access("hasRole('ROLE_ADMIN')")
//                .and()
//                .formLogin().loginPage("/login").failureUrl("/login?error")
//                .usernameParameter("username").passwordParameter("password")
//                .and()
//                .logout().logoutSuccessUrl("/login?logout")
//                .and()
//                .exceptionHandling().accessDeniedPage("/403")
//                .and()
//                .csrf()
           
                ;
        
                http.exceptionHandling().accessDeniedPage("/403");
		http.csrf().disable();
		http.headers().frameOptions().disable();



//                http.httpBasic()
//                        .and()
//                        .authorizeRequests().antMatchers("/h2/**").hasRole("ADMIN").anyRequest().authenticated()
//                        .and()
//                        .authorizeRequests().antMatchers("/object/**").authenticated()
//                        ;
//                
//		http.exceptionHandling().accessDeniedPage("/403");
//		http.csrf().disable();
//		http.headers().frameOptions().disable();

    }
}
