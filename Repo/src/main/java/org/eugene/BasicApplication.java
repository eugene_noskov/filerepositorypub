package org.eugene;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication
public class BasicApplication  {
//extends SpringBootServletInitializer
    
    public static void main(String[] args) {
        
        //SpringApplication.run(BasicApplication.class, args);
        
        Properties prop = new Properties();
        try(InputStream input = new FileInputStream("app-boot.properties")){
            prop.load(input);
            System.setProperty("app_start_profile", prop.getProperty("app.start-profile"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicApplication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BasicApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Two start varians
        //1)
        SpringApplication app = new SpringApplication(BasicApplication.class);
        app.setAdditionalProfiles(System.getProperty("app_start_profile"));
        
        app.setAdditionalProfiles("profile_B");
        
        app.run(args);
        //2
//        //        System Property
//        //        [user@host ~]$ java -jar -Dspring.profiles.active=test myproject.jar
//        //        Program Argument
//        //        [user@host ~]$ java -jar myproject.jar --spring.profiles.active=test
//
//        ApplicationContext appContext = SpringApplication.run(BasicApplication.class, args);


//      ConfigurableEnvironment environment = (ConfigurableEnvironment) appContext.getEnvironment();
//      environment.setActiveProfiles("prod");
                
    }
}
