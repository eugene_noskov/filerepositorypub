package org.eugene.rest.controller;

import org.eugene.rest.dto.Md5AndGuidAnswer;
import org.eugene.rest.dto.AnswerInterface;
import org.eugene.rest.dto.ErrorAnswer;
import org.eugene.persistence.repository.ObjectRepository;
import org.eugene.persistence.entity.Object;
import java.io.File;
import org.eugene.config.AppProperties;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.apachecommons.CommonsLog;
import org.eugene.persistence.entity.EventLog;
import org.eugene.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.eugene.rest.dto.ObjectDto;
import org.eugene.persistence.entity.User;
import org.eugene.persistence.entity.enums.EventEnum;
import org.eugene.persistence.exception.ObjectsNotFoundException;
import org.eugene.persistence.repository.UserRepository;
import org.eugene.persistence.service.EventLogService;
import org.eugene.persistence.service.ObjectService;
import org.eugene.rest.dto.EventLogDto;
import org.eugene.rest.exception.InternalServerErrorException;
import org.eugene.utils.RequestUtils;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@RestController
@CommonsLog
public class Rest_Controller {

//    @Autowired
//    private ApplicationContext appContext;
//    appProperties = appContext.getBean("appProperties", AppProperties.class);
    
    @Autowired
    private ObjectRepository objectRepository;
    
    @Autowired
    RequestUtils sessionUtils;
    
    @Autowired
    private AppProperties appProperties;
    
    @Autowired
    private ObjectService objectService;
    
    @Autowired
    private EventLogService eventLogService;


    //JSON: MD5/GUID
    @RequestMapping(value = "/object/put", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map<String, String>> putObject(
            @RequestParam("description") String description,
            @RequestParam("object") MultipartFile obj
    ) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/octet-stream");

        String uuid =  UUID.randomUUID().toString();

        String fileNameOriginal = "";
        try {
            fileNameOriginal = new String(obj.getOriginalFilename().getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.fatal("Encoding type UTF-8 is not supported", ex);
            throw new InternalServerErrorException("Encoding type UTF-8 is not supported", ex);
        }
        
        Object object = null;
        try {
            byte[] decode = obj.getBytes();
            object = objectService.saveNew(decode, fileNameOriginal, description, uuid);
            eventLogService.saveNew(EventEnum.CREATED, object, description);
        } catch (IOException ex) {
            log.fatal("Не удалось записать файл на сервере", ex);
            throw new InternalServerErrorException("Не удалось записать файл на сервере", ex);
        } catch (NoSuchAlgorithmException ex) {
            log.fatal("Алгоритм шифрования MD5 не найден", ex);
            throw new InternalServerErrorException("Алгоритм шифрования MD5 не найден", ex);
        }
        
        AnswerInterface answer = new Md5AndGuidAnswer(object.getMD5(), uuid);
        
        ResponseEntity<Map<String,String>> response  = new ResponseEntity<>(answer.getAnswer(), headers, HttpStatus.OK);
                
        return response;
    }
    
    
    /**
     * Update file having particular ID
     */
    //JSON: MD5/GUID
    @RequestMapping(value = "/object/setUpdate/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map<String, String>> updateObject(
            @RequestParam("id") String guid,
            @RequestParam("description") String description,
            @RequestParam("object") MultipartFile obj
    ) {

        HttpStatus status = HttpStatus.OK;

        //Setting new filename
        String fileNameOriginal = "";
        try {
            fileNameOriginal = new String(obj.getOriginalFilename().getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.fatal("Encoding is not supported", ex);
            throw new InternalServerErrorException("Encoding is not supported", ex);
        }

        byte[] decode = null;
        try {
           decode = obj.getBytes();
        } catch (IOException ex) {
            log.fatal("Couldn't read bytes from request object", ex);
            throw new InternalServerErrorException("Couldn't read bytes from request object", ex);
        }
        
        Object object = objectService.updateExisted(decode, guid, fileNameOriginal);
        eventLogService.saveNew(EventEnum.UPDATED, object, description);

        AnswerInterface answer = new Md5AndGuidAnswer(object.getMD5(), object.getGUID());

        ResponseEntity<Map<String, String>> response = new ResponseEntity<>(answer.getAnswer(), new HttpHeaders(), status);

        return response;
    }

    
    
    /**
     * Getting object by ID (GUID)
     */
    @RequestMapping(value="/object/get/{id}", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getObject(
                    @PathVariable("id") String guid
            ) throws IOException {
        
        boolean isError = false;
        
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Disposition", ""); 
        
        byte[] answerBytes = null;
        
        Object objectDto = objectRepository.findOne(guid);
        if (objectDto == null) {
            isError = true;
        } else {
            String fileName = Base64.getEncoder().encodeToString(objectDto.getFileNameOriginal().getBytes("UTF-8"));
            
            headers.set("Content-Type", "application/octet-stream");
            headers.set("Content-Transfer-Encoding", "binary");
            AppUtils.setHTTPHeader(headers, "Content-Disposition", "attachment", "");
            AppUtils.setHTTPHeader(headers, "Content-Disposition", "filename", fileName);
            AppUtils.setHTTPHeader(headers, "Content-Disposition", "md5", objectDto.getMD5());

            String fileNamePath = objectDto.getFileNamePath();

            Path path = Paths.get(appProperties.getObjectsFolderLocation() + "/" + fileNamePath);
            answerBytes = Files.readAllBytes(path);
        }
        
        ResponseEntity responseEntity = null;
        
        if(!isError){
            responseEntity = new ResponseEntity<byte[]>(answerBytes, headers, HttpStatus.OK);
        }else{
            String error_mess = headers.get("Content-Disposition") + "; error_msg=\"not found\"";
             headers.set("Content-Disposition", error_mess);
            responseEntity = new ResponseEntity<byte[]>(new byte[0], headers, HttpStatus.NO_CONTENT);
        }
        
        return responseEntity;
    }
    
     /**
     * Getting object by ID (GUID)
     */
    @RequestMapping(value="/object/getInfo", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<List<ObjectDto>> getInfoOfAllObject() throws IOException {
        
        List<ObjectDto> objects = ObjectDto.convertObjectsToDto((List<Object>) objectRepository.findAll());
        
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(objects, headers, HttpStatus.OK);
    }

    
    /**
     * Removing object by ID (GUID)
     */
    @RequestMapping(value="/object/remove/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> removeObject(
                    @PathVariable("id") String guid
            ) {
        
        
        
        ResponseEntity responseEntity = null;
        
        HttpHeaders headers = new HttpHeaders();
        
        try {
            objectService.removeByID(guid);
            responseEntity = new ResponseEntity<String>("", headers, HttpStatus.OK);
        } catch (ObjectsNotFoundException e) {
            responseEntity = new ResponseEntity<String>("", headers, HttpStatus.NO_CONTENT);
        }
        
        
        return responseEntity;
    }

    
    /**
     * Getting object's MD5 by ID (GUID)
     * 
     */
    @RequestMapping(value="/object/getMD5/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map<String,String>> getObjectMD5(
                    @PathVariable("id") String guid
            ) {
        
        Object objectDto = objectRepository.findOne(guid);
        
        ResponseEntity<Map<String,String>> responseEntity = null;
        
        HttpHeaders headers = new HttpHeaders();
        //headers.set("Content-Type", "text/plain;charset=UTF-16");

        AnswerInterface answer = null;        
        if(objectDto != null){
            answer = new Md5AndGuidAnswer(objectDto.getMD5(), guid);
        }else{
            StringBuilder builder = new StringBuilder();
            builder.append("Объект с GUID ").append(guid).append(" не найден; ");
            answer = new ErrorAnswer(builder.toString());
        }
        
        return new ResponseEntity<>(answer.getAnswer(), headers, HttpStatus.OK);
    }
    
    

    @RequestMapping(value="/object/testPost", method = RequestMethod.POST)
    public ResponseEntity<String> testPost(){
        
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> re = new ResponseEntity<>("ok", headers, HttpStatus.NO_CONTENT);
        
        return re;
    }
    
    @RequestMapping(value="/object/testPost/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> testPost(
            @PathVariable("id") String id){
        
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> re = new ResponseEntity<>("ok", headers, HttpStatus.NO_CONTENT);
        
        return re;
    }
    
    @RequestMapping(value="/object/testAction/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> testActionPost(
            @PathVariable("id") String id){
        
        Object obj = objectRepository.findOne(id);
        
        return new ResponseEntity<>("ok", new HttpHeaders(), HttpStatus.OK);
    }
    
    
    /**
    * Getting object's change history by ID (GUID)
    */
    @RequestMapping(value="/object/getChangeLog/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<List<EventLogDto>> getObjectChangeLog(
                                                @PathVariable("id") String id
                                           ) throws IOException {
        
        List<EventLogDto> eventLogDtoList = eventLogService.findMyModelTop20(id);
        
        return new ResponseEntity<>(eventLogDtoList, new HttpHeaders(), HttpStatus.OK);
    }


    
    
//
//      
//    //-----------------------
//    
//    @RequestMapping("/testString")
//    public String getTestString() {
//        return "Greatings!";
//    }
//
//    @RequestMapping("/testObject")
//    public List<testObject> getTestObject() {
//        return Arrays.asList(
//                new testObject("eugene", "noskov"),
//                new testObject("fyodor", "shevchenko")
//        );
//    }
//
////    @RequestMapping("/testFile")
////    public List<testObject> getTestFile() {
////        return Arrays.asList(
////                new testObject("eugene", "noskov"),
////                new testObject("fyodor", "shevchenko")
////        );
////    }
//
//    @RequestMapping("/testFile")
//    public ResponseEntity<byte[]> getImage() throws IOException {
//        String filename = "D:/DOSBox0.74-win32-installer.exe";
//        
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Content-Type", "application/octet-stream");
//        
//        //1
//        Path path = Paths.get(filename);
//        byte[] answerBytes = Files.readAllBytes(path);
//        
//
//        return new ResponseEntity<byte[]>(answerBytes, headers, HttpStatus.OK);
//
//////2
////        InputStream inputImage = new FileInputStream(filename);
////        
////        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////        
////        byte[] buffer = new byte[512];
////        int l = inputImage.read(buffer);
////        
////        while (l >= 0) {
////            outputStream.write(buffer, 0, l);
////            l = inputImage.read(buffer);
////        }
////        
////        return new ResponseEntity<byte[]>(outputStream.toByteArray(), headers, HttpStatus.OK);
//    }
//    
//    @RequestMapping(value = "/uploadBinaryBase64", method = RequestMethod.POST)
//    public String uploadBinaryBase64(
//            @RequestParam("file") String file1
//    ) {
//        byte[] decode = DatatypeConverter.parseBase64Binary(file1);
//        try (FileOutputStream df = new FileOutputStream("D://uploaded.exe")){
//            df.write(decode);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return "ok";
//    }
//    
//    @RequestMapping(value = "/uploadBinaryPOST", method = RequestMethod.POST)
//    public String uploadBinaryPOST(
//            @RequestParam("file1") MultipartFile file1
//    ) {
//        byte[] decode;
//        try (FileOutputStream df = new FileOutputStream("D://uploaded.exe")){
//            decode = file1.getBytes();
//             df.write(decode);
//        } catch (IOException ex) {
//            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return "ok";
//    }
//
//    
////    @RequestMapping(value = "/uploadBinary", method = RequestMethod.POST)
////    public String uploadBinary(@RequestParam("file") MultipartFile file1
////    ) {
////        try {
////            file1.transferTo(new File("d://temp.exe"));
////            return "ok";
////        } catch (IOException ex) {
////            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
////        } catch (IllegalStateException ex) {
////            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
////        }
////        return "nope";
////    }
//
//    
//    @RequestMapping(value = "/sendSimplePost", method = RequestMethod.POST)
//    public String sendSimplePost(
//            @RequestParam("user") String user,
//            @RequestParam("desc") String desc
//            , @RequestParam(value = "datafile", required=false) MultipartFile file1
//    ) {
//        
//           
////        try {
////            byte[] imageByte=Base64.getDecoder().decode(file1.getBytes());
//////            new FileOutputStream("D://uploaded.txt").write(imageByte);
////        } catch (FileNotFoundException ex) {
////            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
////        } catch (IOException ex) {
////            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
////        }
//           
////        try {
////            fos.close();
////        } catch (IOException ex) {
////            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
////        }
//        
//        return "hello " + user + " the " + desc + " - " + file1.toString();
//    }
//    
//    
////    @RequestMapping("/images/{id}")
////    public ResponseEntity<byte[]> getImage(@PathVariable("id") String id) throws IOException {
////        String filename = "D:/images/" + id + ".png";
////        InputStream inputImage = new FileInputStream(filename);
////        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////        byte[] buffer = new byte[512];
////        int l = inputImage.read(buffer);
////        while (l >= 0) {
////            outputStream.write(buffer, 0, l);
////            l = inputImage.read(buffer);
////        }
////        HttpHeaders headers = new HttpHeaders();
////        headers.set("Content-Type", "application/octet-stream");
////        return new ResponseEntity<byte[]>(outputStream.toByteArray(), headers, HttpStatus.OK);
////    }
//
    
}
