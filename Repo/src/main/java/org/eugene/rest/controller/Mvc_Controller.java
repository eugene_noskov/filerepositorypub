package org.eugene.rest.controller;

import javax.websocket.server.PathParam;
import org.eugene.dummy.rest.resource.asm.TestObjectResourceAsm;
import org.eugene.dummy.persistence.entity.TestObject;
import org.eugene.dummy.rest.resource.TestObjectResource;
import org.eugene.dummy.persistence.services.TestObjectService;
import org.eugene.security.UserDetails_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class Mvc_Controller {

    @RequestMapping("/greeting")
    public String greeting(
            @RequestParam(value="name", required=false, defaultValue="man") String name, 
            Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
 
}
