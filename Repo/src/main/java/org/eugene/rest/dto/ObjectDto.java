package org.eugene.rest.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.eugene.persistence.entity.EventLog;

@Data
public class ObjectDto {
    
    private String GUID;
    
    private String fileNamePath;
    
    private String fileNameOriginal;
    
    private String MD5;
    
    private String description;
    
    private String dateLastChange;
    
    private String userLastChange;
    
    private List<EventLog> events;

    public static ObjectDto convertObjectToDto(org.eugene.persistence.entity.Object object){
        
        ObjectDto objectDto = new ObjectDto();
        objectDto.GUID = object.getGUID();
        objectDto.MD5 = object.getMD5();
        objectDto.fileNamePath = object.getFileNamePath();
        objectDto.fileNameOriginal = object.getFileNameOriginal();
        objectDto.description = object.getDescription();
        
        LocalDateTime dateLastChange = object.getDateLastChange();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
        objectDto.dateLastChange = dateLastChange.format(formatter);

        
        objectDto.userLastChange = object.getUserLastChange().getUsername();
        //objectDto.events = object.getEvents();
        
        return objectDto;
    }
    
     public static List<ObjectDto> convertObjectsToDto(List<org.eugene.persistence.entity.Object> objects){
         List<ObjectDto> objectDtos = new ArrayList<>();
         
         for (org.eugene.persistence.entity.Object object : objects) {
            objectDtos.add(ObjectDto.convertObjectToDto(object));
         }
         
         return objectDtos;
     }
    
}
