package org.eugene.rest.dto;

import java.util.LinkedHashMap;
import java.util.Map;

public class Md5AndGuidAnswer implements AnswerInterface{
    
    private Map<String, String> returnMap = new LinkedHashMap();    
    
    public Md5AndGuidAnswer(String md5, String guid){
        returnMap.put("md5", md5);
        returnMap.put("guid", guid);
    }

    @Override
    public Map<String, String> getAnswer() {
        return this.returnMap;
    }
}
