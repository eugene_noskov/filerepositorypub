package org.eugene.rest.dto;

import java.time.LocalDateTime;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import org.eugene.persistence.entity.Event;
import org.eugene.persistence.entity.EventLog;
import org.eugene.persistence.entity.User;
import org.eugene.persistence.entity.Object;
import org.eugene.utils.AppUtils;

@Data
@AllArgsConstructor
public class EventLogDto {

    private String guid;

    private String event;

    private String user;

    private String dateTime;
    
    private String description;
    
    public static EventLogDto convertToDto(EventLog eventLog){
        
        EventLogDto eventLogDto = new EventLogDto(
                eventLog.getObject().getGUID(), 
                eventLog.getEvent().getName(), 
                eventLog.getUser().getUsername(),
                AppUtils.convertDateTimeTo1CDateString(eventLog.getDateTime()),
                eventLog.getDecription()
        );
        
        return eventLogDto;
    }

}
