package org.eugene.rest.dto;

import java.util.LinkedHashMap;
import java.util.Map;

public class ErrorAnswer implements AnswerInterface{
    
    private Map<String, String> returnMap = new LinkedHashMap();    

    public ErrorAnswer(String message) {
        returnMap.put("message", message);
    }

    @Override
    public Map<String, String> getAnswer() {
        return this.returnMap;
    }
    
}
